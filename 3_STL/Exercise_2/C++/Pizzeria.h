#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <sstream>
#include <fstream>



using namespace std;

namespace PizzeriaLibrary {

  class Ingredient{
    public:
      string Name;
      int Price;
      string Description;

    public:
      Ingredient(const string& name, const int& price,const string& description){Name = name; Price = price; Description = description;}
      Ingredient(const string& name){Name = name;}
  };

  class Pizza{
    public:
      string Name;
      list<Ingredient> Ingredients;
    public:
      Pizza(){}
      Pizza(const string& name, const list<Ingredient> ingredients){Name = name; Ingredients = ingredients;}
      void AddIngredient(const Ingredient& ingredient)
      {
          Ingredients.push_back(ingredient);
      }
      int NumIngredients() const
      {
          return Ingredients.size();
      }
      int ComputePrice() const
      {
          unsigned int price = 0;
          list<Ingredient> tmpList = Ingredients;
          for(list<Ingredient>::iterator it = tmpList.begin(); it != tmpList.end(); it++)
          {
              price += it->Price;
          }
          return price;
      }
  };

  class Order {
  public:
      vector <Pizza> Pizzas;
      int NumOrders;
  public:
      void InitializeOrder(int numPizzas, int numOrder)
      {
          Pizzas.reserve(numPizzas);
          NumOrders = numOrder;
      }
      void AddPizza(const Pizza& pizza)
      {
          Pizzas.push_back(pizza);
      }
      const Pizza& GetPizza(const int& position)
      {
          if(position >= Pizzas.size() || position == 0)
              throw runtime_error("Position passed is wrong");
          else
          {
              for(unsigned int i = 0; i < Pizzas.size(); i++)
              {
                  if(i == position - 1)
                  {
                      return Pizzas[i];
                  }
              }
          }
      }
      int NumPizzas() const
      {
          return Pizzas.size();
      }
      int ComputeTotal() const
      {
          unsigned int numpizze = NumPizzas();
          unsigned int price = 0;
          for( unsigned int i = 0; i < numpizze; i++)
          {
              Pizza pizza = Pizzas[i];
              price += pizza.ComputePrice();
          }
          return price;
      }
  };

  class Pizzeria{
    public:
      list<Ingredient> ingredients;
      list<Pizza> pizzas;
      list<Order> orders;
    public:
      void AddIngredient(const string& name,
                         const string& description,
                         const int& price)
      {
          int flag = 0;
          for(list<Ingredient>::iterator it = ingredients.begin();it != ingredients.end(); it++)
          {
              if(it->Name == name)
              {
                  flag = 1;
              }
          }
          if(flag == 1)
          {
              throw runtime_error("Ingredient already inserted");
          }
          else
          {
              Ingredient newIngredient = Ingredient(name);
              newIngredient.Description = description;
              newIngredient.Price = price;
              ingredients.push_back(newIngredient);
          }
      }

      const Ingredient& FindIngredient(const string& name)
      {
          for(list<Ingredient>::iterator it = ingredients.begin();it != ingredients.end(); it++)
          {
              if(it->Name == name)
                  return *it;
          }
          throw runtime_error("Ingredient not found");
      }

      void AddPizza(const string& name,const vector<string>& ingredients)
      {
          for(list<Pizza>::iterator it = pizzas.begin();it != pizzas.end(); it++)
          {
              if(name == it->Name)
                  throw runtime_error("Pizza already inserted");
          }
          Pizza newPizza;
          newPizza.Name = name;
          for(unsigned int i = 0; i < ingredients.size(); i++)
          {
              Ingredient tmpIng = FindIngredient(ingredients[i]);
              newPizza.Ingredients.push_back(tmpIng);
          }
          pizzas.push_back(newPizza);
      }

      const Pizza& FindPizza(const string& name)
      {
          for(list<Pizza>::iterator it = pizzas.begin();it != pizzas.end(); it++)
          {
              if(it->Name == name)
                  return *it;
          }
          throw runtime_error("Pizza not found");
      }
      int CreateOrder(const vector<string>& pizzas)
      {
          if(pizzas.size() == 0)
              throw runtime_error("Empty order");
          unsigned int dimOrder = orders.size();
          Order newOrder;
          for(unsigned int i = 0; i < pizzas.size(); i++)
          {
              Pizza tmpPizza = FindPizza(pizzas[i]);
              newOrder.Pizzas.push_back(tmpPizza);
          }
          newOrder.NumOrders = 1000 + dimOrder;
          orders.push_back(newOrder);
          return newOrder.NumOrders;
      }
      const Order& FindOrder(const int& numOrder)
      {
          for(list<Order>::iterator it = orders.begin(); it != orders.end(); it++)
          {
              if(it->NumOrders == numOrder)
                  return *it;
          }
          throw runtime_error("Order not found");
      }
      string GetReceipt(const int& numOrder)
      {
          string receipt;
          unsigned int ingPrice;
          unsigned int total_ = 0;
          int flag = 0;
          for(list<Order>::iterator it = orders.begin(); it != orders.end() & flag != 1; it++)
          {
              if(it->NumOrders == numOrder)
                  flag = 1;
          }

          if(flag == 1)
          {
              Order fOrder = Pizzeria::FindOrder(numOrder);
              for(unsigned int i = 0; i < fOrder.Pizzas.size(); i++)
              {
                  ingPrice = 0;
                  Pizza fPizza = FindPizza(fOrder.Pizzas[i].Name);
                  for(list<Ingredient>::iterator it = fPizza.Ingredients.begin(); it != fPizza.Ingredients.end(); it++)
                  {
                      Ingredient fIngredient = FindIngredient(it->Name);
                      ingPrice = ingPrice + fIngredient.Price;
                  }
                  if(i == 0)
                      receipt = "- " + fPizza.Name + ", " + to_string(ingPrice) + " euro" + "\n";
                  else
                      receipt = receipt + "- " + fPizza.Name + ", " + to_string(ingPrice) + " euro" + "\n";
                  total_ = total_ + ingPrice;
              }
              receipt = receipt + "  TOTAL: " + to_string(total_) + " euro" + "\n";
              return receipt;
          }

          else
            throw runtime_error("Order not found");
      }

      string ListIngredients()
      {
          vector<string> nameIngredients;
          string ordList;

          for(list<Ingredient>::iterator it = ingredients.begin(); it != ingredients.end(); it++)
              nameIngredients.push_back(it->Name);

          sort(nameIngredients.begin(), nameIngredients.end());
          for(unsigned int i = 0; i < ingredients.size(); i++)
          {
              for(list<Ingredient>::iterator it = ingredients.begin(); it != ingredients.end(); it++)
              {
                  if(nameIngredients[i] == it->Name)
                  {

                      if(i == 0)
                          ordList = it->Name + " - '" + it->Description + "': " + to_string(it->Price) + " euro" + "\n";
                      else
                          ordList = ordList + it->Name + " - '" + it->Description + "': " + to_string(it->Price) + " euro" + "\n";
                  }
              }
          }
          return ordList;
      }
      string Menu()
      {
          string menu;
          int i = -1;
          for(list<Pizza>::iterator it = pizzas.begin(); it != pizzas.end(); it++)
          {
              i++;
              Pizza newPizza = *it;
              unsigned int numberIngredients = newPizza.NumIngredients();
              unsigned int pricePizza = newPizza.ComputePrice();
              if(i == 0)
                  menu = it->Name + " (" + to_string(numberIngredients) + " ingredients): " + to_string(pricePizza) + " euro" + "\n";
              else
                  menu = it->Name + " (" + to_string(numberIngredients) + " ingredients): " + to_string(pricePizza) + " euro" + "\n" + menu;
          }
          return menu;
      }
  };
};

#endif // PIZZERIA_H


