#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

  double Ellipse::Perimeter() const
  {
    double perimeter = 2 * M_PI * (sqrt(((_a * _a) + (_b * _b)) * 0.5 ));
    return  perimeter;
  }

  Triangle::Triangle(const Point& p1,
                     const Point& p2,
                     const Point& p3)
  {
    points.reserve(3);
    points[0] = p1;
    points[1] = p2;
    points[2] = p3;
  }

  double Triangle::Perimeter() const
  {
    double perimeter = 0;
    Point pA = points[0] - points[1];
    Point pB = points[1] - points[2];
    Point pC = points[2] - points[0];

    double a = pA.ComputeNorm2();
    double b = pB.ComputeNorm2();
    double c = pC.ComputeNorm2();
    perimeter = a + b + c;
    return perimeter;
  }

  TriangleEquilateral::TriangleEquilateral(const Point& p1,
                                           const double& edge)
  {
    points.reserve(3);
    points[0] = p1;
    Point p2;
    Point p3;
    p2.X = p1.X + edge * 0.5;
    p2.Y = p1.Y + edge * 0.5 * sqrt(3);
    p3.Y = p1.Y;
    p3.X = p1.X + edge;
    points[1] = p2;
    points[2] = p3;
  }



  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
      points.reserve(4);
      points[0] = p1;
      points[1] = p2;
      points[2] = p3;
      points[3] = p4;
  }

  double Quadrilateral::Perimeter() const
  {
    double perimeter = 0;
    Point pA = points[0] - points[1];
    Point pB = points[1] - points[2];
    Point pC = points[2] - points[3];
    Point pD = points[3] - points[0];

    double a = pA.ComputeNorm2();
    double b = pB.ComputeNorm2();
    double c = pC.ComputeNorm2();
    double d = pD.ComputeNorm2();
    perimeter = a + b + c + d;
    return perimeter;
  }

  Rectangle::Rectangle(const Point& p1,
                       const double& base,
                       const double& height)
  {
      points.reserve(4);
      points[0] = p1;
      Point p2;
      Point p3;
      Point p4;
      p2.Y = p1.Y;
      p2.X = p1.X + base;
      p3.X = p2.X;
      p3.Y = p2.Y + height;
      p4.X = p3.X - base;
      p4.Y = p3.Y;
      points[1] = p2;
      points[2] = p3;
      points[3] = p4;
  }

  Point Point::operator+(const Point& point) const
  {
    Point tempPoint(X, Y);
    tempPoint.X = X + point.X;
    tempPoint.Y = Y + point.Y;
    return tempPoint;
  }

  Point Point::operator-(const Point& point) const
  {
    Point tempPoint(X, Y);
    tempPoint.X = X - point.X;
    tempPoint.Y = Y - point.Y;
    return tempPoint;
  }

  Point&Point::operator-=(const Point& point)
  {
    Point tempPoint(X, Y);
    tempPoint.X = X -= point.X;
    tempPoint.Y = Y -= point.Y;
    return *this;
  }

  Point&Point::operator+=(const Point& point)
  {
    Point tempPoint(X, Y);
    tempPoint.X = X += point.X;
    tempPoint.Y = Y += point.Y;
    return *this;
  }

  Square::Square(const Point &p1, const double &edge)
  {
      points.reserve(4);
      points[0] = p1;
      Point p2;
      Point p3;
      Point p4;
      p2.Y = p1.Y;
      p2.X = p1.X + edge;
      p3.X = p2.X;
      p3.Y = p2.Y + edge;
      p4.X = p3.X - edge;
      p4.Y = p3.Y;
      points[1] = p2;
      points[2] = p3;
      points[3] = p4;
  }

}
