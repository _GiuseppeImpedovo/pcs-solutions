import sys

# \brief ImportText import the text for encryption
# \param inputFilePath: the input file path
# \return the result of the operation, true is success, false is error
# \return text: the resulting text
def importText(inputFilePath):
    file = open(inputFilePath, 'r')
    testo = file.readline()
    text = testo.strip("\n")
    file.close()
    return True, text

# \brief Encrypt encrypt the text
# \param text: the text to encrypt
# \param password: the password for encryption
# \return the result of the operation, true is success, false is error
# \return encryptedText: the resulting encrypted text
def encrypt(text, password):
    global encryptedText
    lenText = len(text)
    lenPassword = len(password)
    j=0
    for i in range (lenText):
        if j == lenPassword:
            j=0
        ascii = ord(text[i]) + ord(password[j])
        if i == 0:
            encryptedText = chr(ascii)
        else:
            encryptedText = encryptedText + chr(ascii)
        j += 1

    return True, encryptedText

# \brief Decrypt decrypt the text
# \param text: the text to decrypt
# \param password: the password for decryption
# \return the result of the operation, true is success, false is error
# \return decryptedText: the resulting decrypted text
def decrypt(text, password):
    lenText = len(text)
    lenPassword = len(password)
    j = 0
    for i in range(lenText):
        if j == lenPassword:
            j = 0
        ascii = ord(text[i]) - ord(password[j])
        if i == 0:
            decryptedText = chr(ascii)
        else:
            decryptedText = decryptedText + chr(ascii)
        j += 1

    return True, decryptedText





if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Password shall passed to the program")
        exit(-1)
    password = sys.argv[1]

    inputFileName = "text.txt"

    [resultImport, text] = importText(inputFileName)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1)
    else:
        print("Import successful: text=", text)

    [resultEncrypt, encryptedText] = encrypt(text, password)
    if not resultEncrypt:
        print("Something goes wrong with encryption")
        exit(-1)
    else:
        print("Encryption successful: result= ", encryptedText)

    [resultEncrypt, decryptedText] = decrypt(encryptedText, password)
    if not resultEncrypt or text != decryptedText:
        print("Something goes wrong with decryption")
        exit(-1)
    else:
        print("Decryption successful: result= ", decryptedText)
