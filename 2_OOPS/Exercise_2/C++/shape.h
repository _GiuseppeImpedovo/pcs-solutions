#ifndef SHAPE_H
#define SHAPE_H
#define _USE_MATH_DEFINES
#include <cmath>

#include <iostream>

using namespace std;

namespace ShapeLibrary
{

  class Point
  {
    public:
      double _x;
      double _y;

    public:
      Point(const double& x, const double& y);
      Point(const Point& point){ }
  };

  class IPolygon
  {
    public:
      virtual double Area() const = 0;
  };

  class Ellipse : public IPolygon
  {
    private:
     Point _center;
     int _a;
     int _b;

    public:
      Ellipse(const Point& center,
              const int& a,
              const int& b): _center(center) { _a = a, _b = b; }


      double Area() const { return M_PI * _a * _b; }
  };

  class Circle : public IPolygon
  {
    private:
      Point _center;
      int _radius;

    public:
      Circle(const Point& center,
             const int& radius): _center(center) { _radius = radius; }

      double Area() const { return M_PI * _radius * _radius; }
  };


  class Triangle : public IPolygon
  {
    private:
      Point _p1;
      Point _p2;
      Point _p3;

    public:
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      double Area() const ;
   };


  class TriangleEquilateral : public IPolygon
  {
    private:
      Point _p1;
      int _edge;

    public:
      TriangleEquilateral(const Point& p1,
                          const int& edge): _p1(p1) {_edge = edge; }

      double Area() const { return sqrt(3)*_edge*_edge/4; }
  };

  class Quadrilateral : public IPolygon
  {
      Point _p1;
      Point _p2;
      Point _p3;
      Point _p4;

    public:
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);

      double Area() const;
  };


  class Parallelogram : public IPolygon
  {
    private:
      Point _p1;
      Point _p2;
      Point _p4;

    public:
      Parallelogram(const Point& p1,
                    const Point& p2,
                    const Point& p4);

      double Area() const;
  };

  class Rectangle : public IPolygon
  {
    private :
      Point _p1;
      int _base;
      int _height;

    public:
      Rectangle(const Point& p1,
                const int& base,
                const int& height): _p1(p1) {_base = base, _height = height; }

      double Area() const { return _height*_base; }
  };

  class Square: public IPolygon
  {
    private:
      Point _p1;
      int _edge;

    public:
      Square(const Point& p1,
             const int& edge): _p1(p1) {_edge = edge; }

      double Area() const { return _edge*_edge; }
  };
}

#endif // SHAPE_H
