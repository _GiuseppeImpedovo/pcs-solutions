#include "Intersector2D1D.h"


Intersector2D1D::Intersector2D1D()
{
    toleranceParallelism = 1.0E-7;
    toleranceIntersection = 1.0E-7;
    intersectionType = Intersector2D1D::NoIntersection;
}

Intersector2D1D::~Intersector2D1D()
{

}

void Intersector2D1D::SetPlane(const Vector3d &planeNormal, const double &planeTranslation)
{
    planeNormalPointer = planeNormal;
    planeTranslationPointer = planeTranslation;
}

void Intersector2D1D::SetLine(const Vector3d &lineOrigin, const Vector3d &lineTangent)
{
    lineOriginPointer = lineOrigin;
    lineTangentPointer = lineTangent;
}

bool Intersector2D1D::ComputeIntersection()
{
    double check1 = planeNormalPointer.dot(lineTangentPointer);
    double check2 = planeNormalPointer.dot(lineOriginPointer);
    if (abs(check1) < toleranceParallelism)
    {
        if (planeTranslationPointer > check2 - toleranceIntersection && planeTranslationPointer < check2 + toleranceIntersection)
        {
            intersectionType = Coplanar;
            return false;
        }
        else
        {
            intersectionType = NoIntersection;
            return false;
        }
    }
    else
    {
        intersectionParametricCoordinate = (planeTranslationPointer - check2) / check1 ;
        intersectionType = PointIntersection;
    }
    return true;
}


